/*	* Name: Durjoy Barua  
 * Student ID: 040919612 
 * Course & Section: CST8132 302 
 * Assignment: Lab 3
 * Date: Oct 1st, 2018 
 */
import java.util.Random;

public class Account
{

	private long accountNum;
	private Client client;
	private double balance;

	Random rand = new Random();


	public Account(Client clnt,double blnc) //Accept client information and balance
	{

		client = clnt;
		balance = blnc;

		accountNum = new Random().nextLong();
	}
	
	public double deposit(double amt) //accept amount and add to the balance
	{
		balance+=amt;
		return balance;
	}


	public boolean withdraw(double amt) //accept amount to check if withdraw is possible
	{		
		if(amt<balance)
		{

			balance-=amt;
			return true;
		}
		else
		{
			return false;
		}
	}

	public long getAccountNum() //return account number
	{
		return accountNum;
	}

	public Client getClient() //return client information
	{
		return client;
	}


	public double getBalance() //return balance
	{
		return balance;
	}


	public String getName() //return client name
	{
		return client.getName();
	}


}

/*	* Name: Durjoy Barua  
 * Student ID: 040919612 
 * Course & Section: CST8132 302 
 * Assignment: Lab 3
 * Date: Oct 1st, 2018 
 */
import java.util.Scanner;
import java.text.DecimalFormat;

public class Bank {

	private String bankName;
	private static Account[] accounts;						  

	Scanner input = new Scanner(System.in);
	DecimalFormat form = new DecimalFormat("#0.00");

	public Bank(){ //default constructor

		System.out.print("Bank Name:"); //input bank name
		bankName = input.next();


		System.out.print("How many accounts you want to set up: "); //input account count
		int i= input.nextInt();
		accounts = new Account[i];

		for(int x = 0; x < i; x++){ //loop for assigning values to the parameter
			System.out.print("Enter your First Name: ");
			String firstName = input.next();

			System.out.print("Enter your Last Name: ");
			String lastName = input.next();

			System.out.print("Enter your Phone Number: ");
			long phoneNum = input.nextLong();

			System.out.print("Enter your Email Address: ");
			String email  = input.next();

			Client client = new Client(firstName, lastName, phoneNum, email); //set the parameter for the client information

			System.out.print("Enter opening balance: ");
			double balance = input.nextDouble();

			Account accNt = new Account(client, balance); //set the values to the accNt object
			accounts[x] = accNt;

			if(accounts[x] == null)
			{
				break;
			}

		}	
	}


	public void printAccounts(){ //method to print the information to the screen

		for(int apt = 0; apt < accounts.length; apt++){
			System.out.print(accounts[apt].getName()+" ");
			System.out.print(accounts[apt].getClient().getEmail()+" ");
			System.out.print(accounts[apt].getClient().getPhoneNum()+" ");
			System.out.print(accounts[apt].getAccountNum()+" ");
			System.out.print(form.format(accounts[apt].getBalance()));
			System.out.println();
		}
	}


	public static void main(String[] args){ //main method to continue the workflow

		Bank bank = new Bank();
		Scanner input = new Scanner(System.in);

		System.out.println("Enter 'p' for the details of account OR 'w' for the withdraw OR 'd' for the deposit OR 'q' to quit: ");
		char s = input.next().charAt(0);


		do{ //loop for the program info
			switch(s){
			case 'P': 
			case 'p':
				bank.printAccounts();
				System.out.println();
				System.out.println("Enter 'p' for the details of account OR 'w' for the withdraw OR 'd' for the deposit OR 'q' to quit: ");
				s= input.next().charAt(0);
				break;
			case 'W':
			case 'w':
				System.out.println("Enter the index no: ");
				int index = input.nextInt();

				while(index>accounts.length)
				{
					System.out.print("Please enter a valid index number: ");
					index = input.nextInt();
				}
				System.out.print("How much money do you want to withdraw? ");
				double amt = input.nextDouble();

				while(accounts[index-1].withdraw(amt) == false){
					accounts[index-1].getBalance();
					System.out.println("Insufficient Funds! Balance is $"+accounts[index-1].getBalance());
					System.out.print("How much money do you want to withdraw? ");
					amt = input.nextDouble();
				}

				System.out.println("Your balance is "+(accounts[index-1].getBalance()-amt));
				System.out.println("Enter 'p' for the details of account OR 'w' for the withdraw OR 'd' for the deposit OR 'q' to quit: ");
				s= input.next().charAt(0);
				break;


			case 'D':
			case 'd':

				System.out.println("Enter the index no: ");
				int index1 = input.nextInt();

				while(index1>accounts.length)
				{
					System.out.print("Please enter a valid index number: ");
					index1 = input.nextInt();
				}
				
				System.out.print("How much money you want to deposit in the account?");
				double deposit  =input.nextDouble();

				System.out.println("Your balance is "+(accounts[index1-1].getBalance()+deposit));
				System.out.println("Enter 'p' for the details of account OR 'w' for the withdraw OR 'd' for the deposit OR 'q' to quit: ");
				s= input.next().charAt(0);
				break;
			}

		}while(s != 'Q' || s!='q');
	}
}

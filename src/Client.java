/*	* Name: Durjoy Barua  
 	* Student ID: 040919612 
 	* Course & Section: CST8132 302 
 	* Assignment: Lab 3
 	* Date: Oct 1st, 2018 
 */
public class Client {
	private String firstName;
	private String lastName;
	private String email;
	private long phoneNum;
	
	public Client(String email, String lastName, long phoneNum, String firstName) { //accept parameter and assign the values
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNum = phoneNum;
		this.email = email;
	}
	
	
	public String getName() { //return concatenated client name
		return (this.firstName+" "+this.lastName);
	}
	
	public long getPhoneNum() { //return phone number
		return this.phoneNum;
	}
	
	public String getEmail() { //return email address
		return this.email;
	}
		
}
